Source: pyresample
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Antonio Valentino <antonio.valentino@tiscali.it>
Section: python
Priority: optional
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-python,
               dh-sequence-numpy3,
               dh-sequence-python3,
               dh-sequence-sphinxdoc,
               pybuild-plugin-pyproject,
               python3-affine <!nocheck>,
               python3-all-dev,
               python3-cartopy (>= 0.20.0),
               python3-configobj,
               python3-dask,
               python3-donfig,
               python3-gdal,
               python3-importlib-metadata,
               python3-matplotlib,
               python3-numexpr,
               python3-numpy,
               python3-pil,
               python3-platformdirs,
               python3-pykdtree,
               python3-pyproj,
               python3-pytest <!nocheck>,
               python3-pytest-lazy-fixtures <!nocheck>,
               python3-rasterio,
               python3-scipy,
               python3-setuptools,
               python3-shapely,
               python3-sphinx <!nodoc>,
               python3-sphinx-reredirects <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               python3-versioneer,
               python3-xarray,
               python3-yaml,
               python3-zarr
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/pyresample
Vcs-Git: https://salsa.debian.org/debian-gis-team/pyresample.git
Homepage: https://github.com/pytroll/pyresample
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild
Description: Resampling of remote sensing data in Python
 Pyresample is a Python package for resampling (reprojection) of earth
 observing satellite data. It handles both resampling of gridded data
 (e.g. geostationary satellites) and swath data (polar orbiting
 satellites).
 .
 Pyresample can use multiple processor cores for resampling.
 Pyresample supports masked arrays.

Package: python3-pyresample
Architecture: any
Depends: python-pyresample-test,
         python3-configobj,
         python3-pyproj,
         python3-pykdtree,
         python3-rasterio,
         python3-yaml,
         ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: python3-affine,
            python3-cartopy,
            python3-dask,
            python3-gdal,
            python3-matplotlib,
            python3-numexpr,
            python3-pil,
            python3-scipy,
            python3-shapely,
            python3-xarray,
            python3-zarr
Suggests: python-pyresample-doc
Description: ${source:Synopsis}
 ${source:Extended-Description}

Package: python-pyresample-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Suggests: www-browser
Description: ${source:Synopsis} (documentation)
 ${source:Extended-Description}
 .
 This is the common documentation package.

Package: python-pyresample-test
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: ${source:Synopsis} (test data)
 ${source:Extended-Description}
 .
 This package contains the test data for pyresample.
